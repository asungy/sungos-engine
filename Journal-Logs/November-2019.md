
November 21, 2019
======
* Again, had a moment yesterday, but I think I'm good now... (we'll see).
* Continuing on with the Keyboard class implementation (it's literally just straight up copying)
* Geez, I'm like a master procrastinator... I was listening to Nier Automata's _City Ruins_, but the Medium version wasn't cutting it for me so I moved over to the dynamic one. Then I just HAD to listen to some intense percussion so I listened to _Song of the Ancients - Fate_, then YouTube suggestions took me into the weeds... but I'm back now (for how long? I don't know...).
  * But seriously... there are some seriously awesome musicians out there. Makes it a great time to be alive.
* But for now, _Persona 5_ OST will keep me company (and boy, is it good company!)
* Lauren called and of course we went into the weeds so I hardly got any new code in (but so worth it).
* I think I finished the Keyboard class
* The episodes just don't stop 😅. And now I have a vendetta 😈
* Moving on to Mouse class implementation.
* I'm just going to finish the Mouse class implementation and move on to Android.

**Tags: #DirectX #Graphics**
___
November 20, 2019
======
* I just got all up in my feels last night and stopped working. But I'm better now.
* Looking at the Keyboard class implementation, Chili deleted the default copy constructor and assignment operator, but if we were to follow the [Rule of Three](https://en.wikipedia.org/wiki/Rule_of_three_(C%2B%2B_programming)), how would we handle the destructor?
* Also last night, I became acutely aware of how dangerous technology can be to someone of my predisposition. I need to manage my technology usage better, including when I'm developing software. And I really do love becoming intimate with the software I develop. But in the modern tech industry, it seems to lack deep personal impact, or at least, the effects of software on people's quality of life is obscure and far removed from the actual work. 
* ... which is not to say that this computer I am typing on has not drastically improved my life and I'm very grateful for all of the efforts to get it into my hands, but perhaps, software as a career is just not suited for me, as a person who is driven by more tangible feedback.
* I don't know what I'm blabbing about... refer to the first 30 seconds of [this video](https://www.youtube.com/watch?v=w5R8gduPZw4) and perhaps the concluding moral of _The Last of Us_ for succinctly expressed emotions on how I feel as far as impact in the tech industry is concerned. 
* It's kind of a hard idea to express with these bullet notes... could be a dedicated blog post or something.
* Anyway...
* Ugh 😪, one last remark. Computers and phones are overly bloated in functionality. In terms of good programming architecture, this is terrible design. Programmers use simple abstraction grab a handle on complex programs. And the phone is segregated into tiny modular apps which makes it easy to navigate the phone. But in the context of life, which is _way too_ overly complex, the phone acts as a panacea for all of our issues because it tries to address everything, whereas it should just be a simple module in life. 

**Tags: #Rant**
___
November 19, 2019
======
* Dummy. I forgot to edit the _wp-config.php_ to point it to my RDS database... fixing that now.
* I still don't know exactly what I'm going to do with my site.
* Finishing up Window class implementation in Sungos
* Switch from Multibyte to Unicode
  * Replacing all chars to TCHARs. It just makes things a little more flexible.
  * See that in a later commit on a the project I'm copying from, I ran into an issue with the character set. I'll deal with it when it comes.
* [Studio Ghibli jazz](https://www.youtube.com/watch?v=3jWRrafhO7M) is pure bliss 😊.
* Finish window class implementation.
* Okay so I think the character set issue I mentioned earlier had to do with the exceptions classes, so I'm going to just use narrow strings and not be so frickin anal.
* Refering to Chili's repo now.
* Added exception handling to Window class (via custom exceptions class)
* ~~SCREW IT, I'M GOING WIDE! 🏈~~
* LOL 😂 never mind... _Exception_ class is derived from _std::exception_ and the parent _what()_ function uses narrow strings.
* Set up icon.

**Tags: #DirectX #Graphics**
___
November 18, 2019
======
* ~~Beginning the game engine series with [The Cherno Project](https://www.youtube.com/playlist?list=PLlrATfBNZ98dC-V-N3m0Go4deliWHPFwT)~~
* So I was going to use OpenGL to create the Sungos Engine because it is multi-platform and what not. But I didn't know that Visual Studio was not available for Linux. Frankly, I just don't want to deal with the hassel of building from the command line, so if I'm going to have to develop on a Windows machine anyway (because I don't do Macs), I mind as well develop the engine with Direct3D since Windows API contains everything I need and I don't have to deal with too many third-party libraries from the start. But the Cherno Project might be a good reference for architectural purposes.
* So [ChiliTomatoNoodle](https://www.youtube.com/playlist?list=PLqCJpWy5Fohd3S7ICFXwUomYW0Wv67pDD) it is!
  * This series is somewhat dependent on his previous series (as far as comprehension is concerned). So there might be some graphics principles that I might not understand. If so, that fine. Try to find it in [this playlist](https://www.youtube.com/playlist?list=PLqCJpWy5Fohe8ucwhksiv9hTF5sfid8lA).
  * Also, I'm going to jot down these books on [O'Reilly Safari](https://www.safaribooksonline.com/library/view/temporary-access/?orpq) for future reference:
    * _Real-Time Rendering, Fourth Edition_
    * _Physically Based Rendering, Third Edition_
* Started new project
  * Removed x86 config
  * Configured output directory
  * Set up gitignore
  * ... more project configuration set up, etc.
* Creating a window
  * Register window class
  * 😂 This Chili dude is awesome
  * Diagram to understand Windows messages: 
    * ![_image not found_](./img/windows-messages.png "Diagram of windows msg pipeline")
  * Created message pump and custom windows procedure (WndProc)
* Going through old implementation of Windows message map class
  * I have done this three times already 😪
  * Debating whether to create a singleton class like before. Read some [stuff](https://stackoverflow.com/questions/137975/what-is-so-bad-about-singletons) on StackOverflow and they're a topic of debate. But I don't think I'm abusing their purpose so I'm just going to stick with singleton implementation. If it turns out to be a problem, I can just change it. It's really not a big deal.
  * Also added Sungos namespace
  * Having no errors and warnings feels real niiiiiice 😎
* This is just a lot code copying, boilerplate shit
* Implementing Window and Window manager class
  * Sometimes I just need to chill out about convention and just write. Like does it really matter if I delete copy and assignment operators of a class if I make the constructor private? I think it's just being verbose at that point... ~~ugh, but I'm going to be explicit anyway~~.
    * Fuck it. Simplicity over verbosity. Just don't be dumb.
    * Refer to [this page](https://en.wikipedia.org/wiki/C++11#Explicitly_defaulted_and_deleted_special_member_functions) if you want a more indepth look at expliciting special member functions
    * "There's no such thing as a perfect program, there's no such thing as a perfect program..."
    * But there are defintely bad programs
    * Never mind... going back on what I said, according to [The Rule of Three](https://en.wikipedia.org/wiki/Rule_of_three_(C%2B%2B_programming)). In the context I'm using these classes I don't think it's a big deal, but I'm just going to assume the rule of three was devised with better insight than I have now. UGH! 😤

**Tags: #DirectX #Graphics**
___
November 17, 2019
======
* Migrating Wordpress database on EC2 instance to RDS
  * Source: _Learn Amazon Web Services in a Month of Lunches, Chapter 4_
  * ![_image not found_](./img/rds-specs.png "RDS calculated cost screenshot")
    * Monthly cost: $14.75
  * Creating databas backup: _mysqldump -u [user] -p wordpressdb > /home/ubuntu/mybackup.sql_
  * If Wordpress site URL changes, need to update config
    * [Instructions here](https://wordpress.org/support/article/changing-the-site-url/)
    * Used RELOCATE method (don't forget to comment or remove afterwards)
  * To migrate backup to RDS instance: _mysql -u [user] -p --database=[database name] --host=[end point URL] < mybackup.sql_
  * [12:37 PM] Ugh... Not paying attention is time consuming. Spent way too much time trying to migrate the database because of stupid mistakes. 
    * I might blame fasting.... Haven't eaten for about 15-16 hours (on purpose)
* [10:01 AM] Been working in calm silence for the past half an hour. It's surprisingly really enjoyable with no music and no pressure.
* Setting up a domain name
  * Source: _Learn Amazon Web Services in a Month of Lunches, Chapter 5_
  * Hosted zone already existed from previous usage
  * Created record set to redirect traffic to elastic IP
  * Created alias record set to enable www- prefix
* Also... I found [this](https://www.youtube.com/watch?v=988UZFB0heA). Should consider going through it for Android project.
* Setting up SSL certificate for aleksung.com (HTTPS)
  * [Source](https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-apache-in-ubuntu-18-04)
  * Created SSL using _OpenSSL_
  * Configured Apache to use SSL cert (_ssl-params.conf, default-ssl.conf_)
  * Going through the steps to redirect HTTP traffic to HTTPS but I think this is already handled with the security groups
  * Maybe should look into how apache works... as for now I'm just going to enable the modules and headers with vague comprehension
  * This tutorial is AMAZING
  * But it's still not working...
  * Relevent from [StackExchange](https://unix.stackexchange.com/questions/358089/apache-ssl-server-cert-does-not-include-id-which-matches-server-name): 
    * > I have followed a simple step-by-step tutorial to create a SSL-certification for my webserver. Like so many tutorials out there the outcome of the tutorial I followed was a self-signed certificate using OpenSSL. Yep self-signed, that was the problem. The browser could not trust the server due to it's certificate which is signed by itself. Well I wouldn't do either...
      >
      > A certificate has to be signed by an external trustworthy certificate authority (CA). So I stumbled upon Let's Encrypt which does all the work for you and is even easier to set up and the best is: it is absolutely free.
    * Deleted OpenSSL self-signed certs
* Trying to set up CertBot... I think I vaguely remember doing this before 🤔
  * [Source](https://www.digitalocean.com/community/tutorials/how-to-secure-apache-with-let-s-encrypt-on-debian-8)
  * Apache Virtual Host file is located at _/etc/apache2/sites-available/000-default.conf_
  * Omg... I love Certbot 😱. That was way too fucking easy!
  * Cert located here: _/etc/letsencrypt/live_
  * ... Redirection not working. WTF???
* Because I'm a fucking idiot, I'm going to create a new EC2 instance and start from scratch...
  * Setting everything up took me like 15 minutes lol... now to try certbot
* Certbot 2nd attempt!
  * [Relevent](https://unix.stackexchange.com/questions/508724/failed-to-fetch-jessie-backports-repository): Jessie backport repo has been relocated but the new repo isn't signed so... but I don't think this is really a problem. Carrying on.
  * Nier Automata's _Wretched Weaponry_ is soooo beautiful... The whole fucking soundtrack is beautiful.
  * Yup... it fuggin works now (sorry, lot's of eff-words). The self-signing certificate process probably messed my apache config somehow...
  * Set up certificate auto-renewal
* Need to have HTTP in security group and let apache redirect to HTTPS, otherwise aleksung.com does not redirect to HTTPS
* Created health check for site

**Tags: #AWS #SSL #Apache #Certbot**
___
November 16, 2019
======
* Learning markdown
  * Source: [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* Creating EC2 instance for Wordpress site
  * Source: _Learn Amazon Web Services in a Month of Lunches, Chapter 2_
  * This is a t2-micro Ubuntu 18.04 LTS server which will cost about $8.50, running at 100% per month
  * Successfully set up web server with default Apache page
* Listening to [Song of the Ancients - Popola](https://www.youtube.com/watch?v=PTJSCJGIv2s)
  * Oh my gawd... this music is pure divinity
* Installing Wordpress on EC2 instance
  * Source: _Learn Amazon Web Services in a Month of Lunches, Chapter 3_
  * Creating SQL Database:
    * _$> mysql -u root -p_
    * _mysql> CREATE DATABASE [database name];_
    * _mysql> CREATE USER '[user]'@'localhost' IDENTIFIED BY '[password]';_
    * _mysql> GRANT ALL PRIVILEGES ON wordpressdb.* To '[user]'@'localhost';_
    * _mysql> FLUSH PRIVILEGES;_
    * _mysql> exit_
  * Since I'm writing these commands down for reference I think I should consider learning a little about SQL
    * Consider: No Starch Press, _Practical SQL_
  * Note: not all steps and commands will be included in these entries. These entries are just for loose record keeping.
  * Edit _wp-config.php_ and added salts
  * Set up Wordpress successfully
* Need to set up SSL on server

**Tags: #AWS #MySQL #Wordpress #Markdown**
___