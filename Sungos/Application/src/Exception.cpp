#include "Exception.h"

#include <sstream>

namespace Sungos {

	Exception::Exception( int lineNum, const char * fileName ) noexcept
		:
		lineNum(lineNum),
		fileName(fileName)
	{}

	const char * Exception::what() const noexcept
	{
		std::ostringstream stream;
		stream << GetType() << std::endl << GetOriginString();
		whatBuffer = stream.str();
		return whatBuffer.c_str();
	}

	const char * Exception::GetType() const noexcept
	{
		return "Exception";
	}

	int Exception::GetLineNum() const noexcept
	{
		return lineNum;
	}

	const std::string & Exception::GetFileName() const noexcept
	{
		return fileName;
	}

	std::string Exception::GetOriginString() const noexcept
	{
		std::ostringstream stream;
		stream << "[File] " << fileName << std::endl
			<< "[Line] " << lineNum;
		return stream.str();
	}
}
