#pragma once
#include <queue>

namespace Sungos
{
	class Mouse
	{
		friend class Window;

	public:
		class Event
		{
		public:
			enum class Type
			{
				LeftPress,
				LeftRelease,
				RightPress,
				RightRelease,
				WheelUp,
				WheelDown,
				Move,
				Invalid
			};

		private:
			Type type;
			bool leftIsPressed;
			bool rightIsPressed;

			int xPos;
			int yPos;

		public:
			Event() noexcept;
			Event( Type type, const Mouse & parent ) noexcept;

			bool IsValid() const noexcept;
			Type GetType() const noexcept;
			std::pair<int, int> GetPosition() const noexcept;
			int GetPosX() const noexcept;
			int GetPosY() const noexcept;
			bool LeftIsPressed() const noexcept;
			bool RightIsPressed() const noexcept;
		};

	public:
		Mouse() = default;
		Mouse( const Mouse & ) = delete;
		Mouse & operator=( const Mouse & ) = delete;
		std::pair<int, int> GetPosition() const noexcept;
		int GetPosX() const noexcept;
		int GetPosY() const noexcept;
		bool LeftIsPressed() const noexcept;
		bool RightIsPressed() const noexcept;
		Mouse::Event Read() noexcept;
		bool IsEmpty() const noexcept;
		void Flush() noexcept;

	private:
		void OnMouseMove( int newX, int newY ) noexcept;
		void OnLeftPressed( int newX, int newY ) noexcept;
		void OnLeftReleased( int newX, int newY ) noexcept;
		void OnRightPressed( int newX, int newY ) noexcept;
		void OnRightReleased( int newX, int newY ) noexcept;
		void OnWheelUp( int newX, int newY ) noexcept;
		void OnWheelDown( int newX, int newY ) noexcept;
		void TrimBuffer() noexcept;

	private:
		static constexpr unsigned int bufferSize = 16u;
		int xPos;
		int yPos;
		bool leftIsPressed = false;
		bool rightIsPressed = false;
		std::queue<Event> buffer;
	};
}

