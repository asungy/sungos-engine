#include "Window.h"

#include "resource.h"

#include <sstream>

namespace Sungos
{
	Window::Manager * Window::Manager::manager = NULL;

	Window::Manager * Window::Manager::Instance()
	{
		if ( manager == NULL )
			manager = new Window::Manager();

		return manager;
	}

	const TCHAR * Window::Manager::GetName() const noexcept
	{
		return windowClassName;
	}

	HINSTANCE Window::Manager::GetInstance() const noexcept
	{
		return hInstance;
	}

	Window::Manager::Manager() noexcept
		:
		hInstance( GetModuleHandle( nullptr ) )
	{
		WNDCLASSEX wc = { 0 };
		wc.cbSize = sizeof( wc );
		wc.style = CS_OWNDC;
		wc.lpfnWndProc = HandleMsgSetup;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = hInstance;
		wc.hIcon = static_cast<HICON>( LoadImage(
			GetInstance(),
			MAKEINTRESOURCE( IDI_APPICON ),
			IMAGE_ICON,
			0, 0,
			LR_DEFAULTSIZE
			) );
		wc.hCursor = nullptr;
		wc.hbrBackground = nullptr;
		wc.lpszMenuName = nullptr;
		wc.lpszClassName = GetName();
		RegisterClassEx( &wc );
	}

	Window::Manager::~Manager()
	{
		UnregisterClass( GetName(), GetInstance() );
	}

	Window::Window(
		const TCHAR * name,
		int width,
		int height,
		int left,
		int top
	)
		:
		width( width ),
		height( height ),
		hWnd( NULL )
	{
		// Set window dimensions
		RECT rect;
		rect.left = left;
		rect.right = width + left;
		rect.top = top;
		rect.bottom = height + top;

		if ( AdjustWindowRect( &rect, WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU, FALSE ) == 0 ) 
			throw WINDOW_LAST_EXCEPT();

		hWnd = CreateWindow(
			Window::Manager::Instance()->GetName(),
			name,
			WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			rect.right - rect.left,
			rect.bottom - rect.top,
			nullptr,
			nullptr,
			Window::Manager::Instance()->GetInstance(),
			this
		);

		if ( hWnd == nullptr )
			throw WINDOW_LAST_EXCEPT();

		ShowWindow( hWnd, SW_SHOWDEFAULT );
	}

	Window::~Window()
	{
		DestroyWindow( hWnd );
	}

	LRESULT Window::HandleMsgSetup( 
		HWND hWnd, 
		UINT msg, 
		WPARAM wParam, 
		LPARAM lParam 
	) noexcept
	{
		if ( msg == WM_NCCREATE )
		{
			// Extract Window class pointer from CreateWindow call in Window constructor
			const CREATESTRUCT * const pCreate = reinterpret_cast<CREATESTRUCT *>( lParam );
			Window * const pWnd = static_cast<Window *>( pCreate->lpCreateParams );

			// Set WinAPI-managed user data to store pointer of Window class instance
			SetWindowLongPtr( hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>( pWnd ) );

			// Set window process from registered window instance to message thunk handler
			SetWindowLongPtr( hWnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>( &Window::HandleMsgThunk ) );

			// Forward message to window instance handler
			return pWnd->HandleMsg( hWnd, msg, wParam, lParam );
		}

		// Handle any messages before WM_NCCREATE message with default handler
		return DefWindowProc( hWnd, msg, wParam, lParam );
	}

	LRESULT Window::HandleMsgThunk( 
		HWND hWnd, 
		UINT msg, 
		WPARAM wParam, 
		LPARAM lParam 
	) noexcept
	{
		// Retrieve pointer to Window instance
		Window * const pWnd = reinterpret_cast<Window *>( GetWindowLongPtr( hWnd, GWLP_USERDATA ) );

		// Forward message to Window instance handler
		return pWnd->HandleMsg( hWnd, msg, wParam, lParam );
	}

	LRESULT Window::HandleMsg( 
		HWND hWnd, 
		UINT msg, 
		WPARAM wParam, 
		LPARAM lParam 
	) noexcept
	{
		switch ( msg )
		{
		case WM_CLOSE:
			PostQuitMessage( 0 );
			return 0;

		/******************** Keyboard Messages ***********************/
		case WM_KEYDOWN:
		case WM_SYSKEYDOWN:
			// Auto-repeat filter
			if (!(lParam & 0x40000000) || keyboard.AutorepeatIsEnabled() )
				keyboard.OnKeyPressed( static_cast<unsigned char>( wParam ) );
			break;

		case WM_KEYUP:
		case WM_SYSKEYUP:
			keyboard.OnKeyReleased( static_cast<unsigned char>( wParam ) );
			break;

		case WM_CHAR:
			keyboard.OnChar( static_cast<unsigned char>( wParam ) );
			break;

		case WM_KILLFOCUS:
			keyboard.ResetKeyStates();
			break;
		/******************** End of Keyboard Messages ****************/

		/******************** Mouse Messages **************************/
		case WM_MOUSEMOVE:
		{
			const POINTS pt = MAKEPOINTS( lParam );
			mouse.OnMouseMove( pt.x, pt.y );
			break;
		}

		case WM_LBUTTONDOWN:
		{
			const POINTS pt = MAKEPOINTS( lParam );
			mouse.OnLeftPressed( pt.x, pt.y );
			break;
		}

		case WM_RBUTTONDOWN:
		{
			const POINTS pt = MAKEPOINTS( lParam );
			mouse.OnRightPressed( pt.x, pt.y );
			break;
		}

		case WM_LBUTTONUP:
		{
			const POINTS pt = MAKEPOINTS( lParam );
			mouse.OnLeftPressed( pt.x, pt.y );
			break;

		}

		case WM_RBUTTONUP:
		{
			const POINTS pt = MAKEPOINTS( lParam );
			mouse.OnRightReleased( pt.x, pt.y );
			break;
		}

		case WM_MOUSEWHEEL:
		{
			const POINTS pt = MAKEPOINTS( lParam );
			if ( GET_WHEEL_DELTA_WPARAM( wParam ) > 0 )
				mouse.OnWheelUp( pt.x, pt.y );
			else if ( GET_WHEEL_DELTA_WPARAM( wParam ) < 0 )
				mouse.OnWheelDown( pt.x, pt.y );
			break;
		}
		/******************** End of Mouse Messages *******************/
		}

		// Handle all other outputs with default handler
		return DefWindowProc( hWnd, msg, wParam, lParam );
	}

	Window::WindowException::WindowException( 
		int lineNum, 
		const char * fileName, 
		HRESULT hr 
	) noexcept
		:
		Exception(lineNum, fileName),
		hr(hr)
	{}

	const char * Window::WindowException::what() const noexcept
	{
		std::ostringstream stream;
		stream << GetType() << std::endl
			<< "[Error Code] " << GetErrorCode() << std::endl
			<< "[Description] " << GetErrorString() << std::endl
			<< GetOriginString();
		whatBuffer = stream.str();
		return whatBuffer.c_str();
	}

	const char * Window::WindowException::GetType() const noexcept
	{
		return "Window Exception";
	}

	std::string Window::WindowException::TranslateErrorCode( HRESULT hr ) noexcept
	{
		char * pMsgBuf = nullptr;
		DWORD nMsgLen = FormatMessageA(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
			nullptr,
			hr,
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			reinterpret_cast<LPSTR>( &pMsgBuf ), 
			0,
			nullptr
		);

		if ( nMsgLen == 0 )
			return "Unidentified error code";

		std::string errorString = pMsgBuf;
		LocalFree( pMsgBuf );
		return errorString;
	}

	HRESULT Window::WindowException::GetErrorCode() const noexcept
	{
		return hr;
	}

	std::string Window::WindowException::GetErrorString() const noexcept
	{
		return TranslateErrorCode( hr );
	}
}
