#include "Keyboard.h"

namespace Sungos {
	Keyboard::Event::Event() noexcept
		:
		type( Type::Invalid ),
		code( 0u )
	{}

	Keyboard::Event::Event( Type type, unsigned char code ) noexcept
		:
		type( type ),
		code( code )
	{}

	bool Keyboard::Event::IsPressed() const noexcept
	{
		return type == Type::Pressed;
	}

	bool Keyboard::Event::IsReleased() const noexcept
	{
		return type == Type::Released;
	}

	bool Keyboard::Event::IsValid() const noexcept
	{
		return type != Type::Invalid;
	}

	unsigned char Keyboard::Event::GetCode() const noexcept
	{
		return code;
	}

	bool Keyboard::KeyIsPressed( unsigned char keycode ) const noexcept
	{
		return keyStates[keycode];
	}

	Keyboard::Event Keyboard::ReadKey() noexcept
	{
		if ( keyBuffer.size() > 0u )
		{
			Keyboard::Event ke = keyBuffer.front();
			keyBuffer.pop();
			return ke;
		}
		else
		{
			return Keyboard::Event();
		}
	}

	bool Keyboard::KeyBufferIsEmpty() const noexcept
	{
		return keyBuffer.empty();
	}

	void Keyboard::ClearKeyBuffer() noexcept
	{
		keyBuffer = std::queue<Event>();
	}

	char Keyboard::ReadChar() noexcept
	{
		if ( charBuffer.size() > 0u )
		{
			unsigned char charcode = charBuffer.front();
			charBuffer.pop();
			return charcode;
		}
		else
		{
			return 0;
		}
	}

	bool Keyboard::CharIsEmpty() const noexcept
	{
		return charBuffer.empty();
	}

	void Keyboard::FlushCharBuffer() noexcept
	{
		charBuffer = std::queue<char>();
	}

	void Keyboard::ClearBuffers() noexcept
	{
		ClearKeyBuffer();
		FlushCharBuffer();
	}

	void Keyboard::EnableAutorepeat() noexcept
	{
		autorepeatEnabled = true;
	}

	void Keyboard::DisableAutorepeat() noexcept
	{
		autorepeatEnabled = false;
	}

	bool Keyboard::AutorepeatIsEnabled() noexcept
	{
		return autorepeatEnabled;
	}

	void Keyboard::OnKeyPressed( unsigned char keycode ) noexcept
	{
		keyStates[keycode] = true;
		keyBuffer.push( Keyboard::Event( Keyboard::Event::Type::Pressed, keycode ) );
		TrimBuffer( keyBuffer );
	}

	void Keyboard::OnKeyReleased( unsigned char keycode ) noexcept
	{
		keyStates[keycode] = true;
		keyBuffer.push( Keyboard::Event( Keyboard::Event::Type::Released, keycode ) );
		TrimBuffer( keyBuffer );
	}

	void Keyboard::OnChar( char character ) noexcept
	{
		charBuffer.push( character );
		TrimBuffer( charBuffer );
	}

	void Keyboard::ResetKeyStates() noexcept
	{
		keyStates.reset();
	}

	template<typename T>
	void Keyboard::TrimBuffer( std::queue<T> & buffer ) noexcept
	{
		while ( buffer.size() > bufferSize )
			buffer.pop();
	}
}
