#pragma once

#include "LimitWin.h"

#include "Exception.h"
#include "Keyboard.h"
#include "Mouse.h"

/*
	Class to handle the viewport for the rendering engine
*/

namespace Sungos
{
	class Window
	{
	public:
		class WindowException : public Exception
		{
		public:
			WindowException( int lineNum, const char * fileName, HRESULT hr ) noexcept;
			const char * what() const noexcept override;
			virtual const char * GetType() const noexcept override;
			static std::string TranslateErrorCode( HRESULT hr ) noexcept;
			HRESULT GetErrorCode() const noexcept;
			std::string GetErrorString() const noexcept;

		private:
			HRESULT hr;
		};

	private:
		// Singleton class to manage registration/cleanup for Window class
		class Manager
		{
		public:
			static Manager * Instance();

			const TCHAR * GetName() const noexcept;
			HINSTANCE GetInstance() const noexcept;

		private:
			Manager() noexcept;

			~Manager();
			Manager( const Manager & ) = delete;
			Manager & operator=( const Manager & ) = delete;
			Manager( Manager && ) = delete;
			Manager & operator=( const Manager && ) = delete;

		private:
			static Manager * manager;

			HINSTANCE hInstance;
			static constexpr const TCHAR * windowClassName = TEXT("Sungos Viewport Window");
		};
	public:
		Window( const TCHAR * name, int width, int height, int left = 100, int top = 100 );

		~Window();
		Window( const Window & ) = delete;
		Window & operator=( const Window & ) = delete;
		Window( Window && ) = delete;
		Window & operator=( const Window && ) = delete;

	private:
		// Initial procedure to be used in window class registration
		static LRESULT CALLBACK HandleMsgSetup( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam ) noexcept;
		// Wrapper for Window class member function, Window::HandMsg
		static LRESULT CALLBACK HandleMsgThunk( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam ) noexcept;
		LRESULT HandleMsg( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam ) noexcept;

	public:
		Keyboard keyboard;
		Mouse mouse;

	private:
		int width;
		int height;
		HWND hWnd;
	};
}

#define WINDOW_EXCEPTION( hr ) Window::WindowException( __LINE__, __FILE__, hr)
#define WINDOW_LAST_EXCEPT() Window::WindowException( __LINE__, __FILE__, GetLastError() )
