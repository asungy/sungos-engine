#include "Mouse.h"

namespace Sungos
{

	Mouse::Event::Event() noexcept
		:
		type( Type::Invalid ),
		leftIsPressed( false ),
		rightIsPressed( false ),
		xPos( 0 ),
		yPos( 0 )
	{}
	Mouse::Event::Event( Type type, const Mouse & parent ) noexcept
		:
		type(type),
		leftIsPressed(parent.leftIsPressed),
		rightIsPressed(parent.rightIsPressed),
		xPos(parent.GetPosX()),
		yPos(parent.GetPosY())
	{}

	bool Mouse::Event::IsValid() const noexcept
	{
		return type != Type::Invalid;
	}

	Mouse::Event::Type Mouse::Event::GetType() const noexcept
	{
		return type;
	}

	std::pair<int, int> Mouse::Event::GetPosition() const noexcept
	{
		return { xPos, yPos };
	}

	int Mouse::Event::GetPosX() const noexcept
	{
		return xPos;
	}

	int Mouse::Event::GetPosY() const noexcept
	{
		return yPos;
	}

	bool Mouse::Event::LeftIsPressed() const noexcept
	{
		return leftIsPressed;
	}

	bool Mouse::Event::RightIsPressed() const noexcept
	{
		return rightIsPressed;
	}

	std::pair<int, int> Mouse::GetPosition() const noexcept
	{
		return { xPos, yPos };
	}

	int Mouse::GetPosX() const noexcept
	{
		return xPos;
	}

	int Mouse::GetPosY() const noexcept
	{
		return yPos;
	}

	bool Mouse::LeftIsPressed() const noexcept
	{
		return leftIsPressed;
	}

	bool Mouse::RightIsPressed() const noexcept
	{
		return rightIsPressed;
	}

	Mouse::Event Mouse::Read() noexcept
	{
		if ( buffer.size() > 0u )
		{
			Mouse::Event e = buffer.front();
			buffer.pop();
			return e;
		}
		else
		{
			return Mouse::Event();
		}
	}

	bool Mouse::IsEmpty() const noexcept
	{
		return buffer.empty();
	}

	void Mouse::Flush() noexcept
	{
		buffer = std::queue<Event>();
	}

	void Mouse::OnMouseMove( int newX, int newY ) noexcept
	{
		xPos = newX;
		yPos = newY;

		buffer.push( Mouse::Event( Mouse::Event::Type::Move, *this ) );
		TrimBuffer();
	}

	void Mouse::OnLeftPressed( int newX, int newY ) noexcept
	{
		leftIsPressed = true;

		buffer.push( Mouse::Event( Mouse::Event::Type::LeftPress, *this ) );
		TrimBuffer();
	}

	void Mouse::OnLeftReleased( int newX, int newY ) noexcept
	{
		leftIsPressed = false;
		
		buffer.push( Mouse::Event( Mouse::Event::Type::LeftRelease, *this ) );
		TrimBuffer();
	}

	void Mouse::OnRightPressed( int newX, int newY ) noexcept
	{
		rightIsPressed = true;

		buffer.push( Mouse::Event( Mouse::Event::Type::RightPress, *this ) );
		TrimBuffer();
	}

	void Mouse::OnRightReleased( int newX, int newY ) noexcept
	{
		rightIsPressed = false;

		buffer.push( Mouse::Event( Mouse::Event::Type::RightRelease, *this ) );
		TrimBuffer();
	}

	void Mouse::OnWheelUp( int newX, int newY ) noexcept
	{
		buffer.push( Mouse::Event( Mouse::Event::Type::WheelUp, *this ) );
		TrimBuffer();
	}

	void Mouse::OnWheelDown( int newX, int newY ) noexcept
	{
		buffer.push( Mouse::Event( Mouse::Event::Type::WheelDown, *this ) );
		TrimBuffer();
	}

	void Mouse::TrimBuffer() noexcept
	{
		while ( buffer.size() > bufferSize )
			buffer.pop();
	}
}
