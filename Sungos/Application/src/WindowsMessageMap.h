#pragma once

#include "LimitWin.h"

#include <unordered_map>

/*
	This singleton class contains a map of Windows messages and
	their respective unique identifying number.
*/

namespace Sungos
{
	class WindowsMessageMap
	{
	public:
		static WindowsMessageMap * Instance();
		std::string MessageInfo( DWORD msg, LPARAM lParam, WPARAM wParam ) const;

	private:
		WindowsMessageMap();

	private:
		static WindowsMessageMap * instance;
		std::unordered_map<DWORD, std::string> map;
	};
}

