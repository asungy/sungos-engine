#pragma once
#include <queue>
#include <bitset>

namespace Sungos
{
	class Keyboard
	{
		friend class Window;

	public:
		class Event
		{
		public:
			enum class Type
			{
				Pressed,
				Released,
				Invalid
			};

		private:
			Type type;
			unsigned char code;

		public:
			Event() noexcept;
			Event( Type type, unsigned char code ) noexcept;

			bool IsPressed() const noexcept;
			bool IsReleased() const noexcept;
			bool IsValid() const noexcept;
			unsigned char GetCode() const noexcept;
		};
	public:
		Keyboard() = default;
		Keyboard( const Keyboard & ) = delete;
		Keyboard & operator=( const Keyboard & ) = delete;

		// Key event related
		bool KeyIsPressed( unsigned char keycode ) const noexcept;
		Event ReadKey() noexcept;
		bool KeyBufferIsEmpty() const noexcept;
		void ClearKeyBuffer() noexcept;

		// Char event related
		char ReadChar() noexcept;
		bool CharIsEmpty() const noexcept;
		void FlushCharBuffer() noexcept;
		void ClearBuffers() noexcept;

		// Auto-repeat control
		void EnableAutorepeat() noexcept;
		void DisableAutorepeat() noexcept;
		bool AutorepeatIsEnabled() noexcept;

	private:
		void OnKeyPressed( unsigned char keycode ) noexcept;
		void OnKeyReleased( unsigned char keycode ) noexcept;
		void OnChar( char character ) noexcept;
		void ResetKeyStates() noexcept;
		template<typename T>
		static void TrimBuffer( std::queue<T> & buffer ) noexcept;

	private:
		static constexpr unsigned int nKeys = 256u;
		static constexpr unsigned int bufferSize = 16u;
		bool autorepeatEnabled = false;
		std::bitset<nKeys> keyStates;
		std::queue<Event>  keyBuffer;
		std::queue<char>   charBuffer;
	};
}
