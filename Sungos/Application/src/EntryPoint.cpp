#include "LimitWin.h"

#include "Window.h"
#include "WindowsMessageMap.h"

#include <sstream>

using namespace Sungos;

int CALLBACK WinMain(
	_In_ HINSTANCE     hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPSTR         lpCmdLine,
	_In_ int           nCmdShow
)
{
	try
	{
		Window window( TEXT( "Sungos" ), 800, 300 );

		// message pump
		MSG message;
		BOOL result;
		while ( result = ( GetMessage( &message, nullptr, 0, 0 ) ) > 0 )
		{
			TranslateMessage( &message );
			DispatchMessage( &message );
		}

		if ( result == -1 )
			throw WINDOW_LAST_EXCEPT();

		return (int) message.wParam;
	}
	catch ( const Exception & e )
	{
		MessageBoxA( nullptr, e.what(), e.GetType(), MB_OK | MB_ICONEXCLAMATION );
	}
	catch ( const std::exception & e )
	{
		MessageBoxA( nullptr, e.what(), "Standard Exception", MB_OK | MB_ICONEXCLAMATION );
	}
	catch ( ... )
	{
		MessageBoxA( nullptr, "No details available", "Unknown Exception", MB_OK | MB_ICONEXCLAMATION );
	}

	return -1;
}