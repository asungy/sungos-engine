#pragma once

#include <exception>
#include <string>

namespace Sungos {
	class Exception : public std::exception
	{
	public:
		Exception( int lineNum, const char * fileName ) noexcept;

		const char * what() const noexcept override;
		virtual const char * GetType() const noexcept;
		int GetLineNum() const noexcept;
		const std::string & GetFileName() const noexcept;
		std::string GetOriginString() const noexcept;

	private:
		int lineNum;
		std::string fileName;

	protected:
		mutable std::string whatBuffer;
	};
}
